import JSONProducts from './products.json';

const ProductsService = {
  getProducts() {
    const products = JSONProducts;

    return products.products;
  },

  getProduct(id) {
    let products = JSONProducts;
    products = products.products;
    const result = products.filter(product => product.id == id);

    return result[0];
  }
};

export default ProductsService;