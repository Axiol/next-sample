import React, { Component } from 'react';
import styled from 'styled-components';

import Product from '../components/Product';
import ProductsService from '../services/Products';

const ProductsWrapper = styled.div`
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	grid-column-gap: 30px;
`;

class Index extends Component {
	state = {
		products: []
	}

	componentDidMount() {
		const products = ProductsService.getProducts();

		this.setState({ products: [...products] });
	}

	render() {
		return (
			<div>
				<h2>Liste des produits</h2>

				<ProductsWrapper>
					{ this.state.products.map(product => <Product key={product.id} id={product.id} name={product.name} image={product.image} />) }
				</ProductsWrapper>
			</div>
		);
	}
}

export default Index;