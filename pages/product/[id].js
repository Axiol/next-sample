import React, { Component } from 'react';
import { withRouter } from 'next/router';
import styled from 'styled-components';

import ProductsService from '../../services/Products';

class ProductPage extends Component {
	state = {
		product: []
	}

	componentDidMount() {
		console.log(this.props.router.query.id);
		const product = ProductsService.getProduct(this.props.router.query.id);

		this.setState({ product: product });
	}

	render() {
		return (
			<div>
				<h2>{ this.state.product.name }</h2>
				<img src={ this.state.product.image } />
				<p>{ this.state.product.description }</p>
				<p><button>Ajouter au panier</button></p>
			</div>
		);
	}
}

export default withRouter(ProductPage);