import React, { Component } from 'react';
import styled from 'styled-components';

import Meta from './Meta';
import Header from './Header';

const Wrapper = styled.div`
  font-family: sans-serif;
  max-width: 1000px;
  margin: 0 auto;
`;

class Page extends Component {
  render() {
    return (
      <div>
        <Meta />

        <Header />

        <Wrapper>
          { this.props.children }
        </Wrapper>
      </div>
    );
  }
}

export default Page;