import Link from 'next/link';
import styled from 'styled-components';

const ProductBox = styled.div`
  box-shadow: 0 0 10px #0000002e;
  border-radius: 3px;
  overflow: hidden;
  margin-bottom: 40px;
  position: relative;

  img {
    width: 100%;
  }

  h3 {
    padding: 0 20px;
  }

  a {
    position: absolute;
    top: 0; right: 0; bottom: 0; left: 0;
    text-indent: -9999px;
  }
`;

export default function Product(props) {
	return (
		<ProductBox>
      <img src={ props.image } />
      <h3>{ props.name }</h3>
      <Link href="/product/[id]" as={ `/product/${props.id}` }>
        <a>Détails</a>
      </Link>
		</ProductBox>
	);
}