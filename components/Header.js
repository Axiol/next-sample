import Link from 'next/link';

import styled from 'styled-components';

const LayoutHeader = styled.header`
  border-bottom: 1px solid lightgray;
  padding: 5px 0 10px 0;
  margin-bottom: 40px;
`;

export default function Header() {
	return (
		<LayoutHeader>
      <Link href="/">
        <img src="https://via.placeholder.com/150x80.png?text=Logo" />
      </Link>
		</LayoutHeader>
	);
}